package com.epam.training.fooddelivery.service;

import com.epam.training.fooddelivery.data.DataStore;
import com.epam.training.fooddelivery.domain.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public class DefaultFoodDeliveryService implements FoodDeliveryService {
    private DataStore dataStore;

    public DefaultFoodDeliveryService(DataStore dataStore) {
        this.dataStore = dataStore;
    }

    @Override
    public Customer authenticate(User user) {
        Customer customer = dataStore.getCustomers()
                .stream()
                .filter(ele -> ele.getEmail().equals(user.getEmail())
                        && ele.getPassword().equals(user.getPassword()))
                .findFirst()
                .orElseThrow(() -> new AuthenticationException("Invalid username or password"));
        return customer;
    }

    @Override
    public List<Food> listAllFood() {
        return dataStore.getFoods();
    }

    @Override
    public void updateCart(Customer customer, Food food, int pieces) {
        Cart cart = getCart(customer);
        if (pieces == 0) {
            extractedOldOrder(food, cart);
        } else {
            BigDecimal price = BigDecimal.valueOf(pieces).multiply(food.getPrice());
            OrderItem item = createOrderItem(food, pieces, price);
            if (cart.getOrderItems() != null) {
                extractedOldOrder(food, cart);
            }
            cart.setOrderItems(List.of(item));
            cart.setPrice(cart.getPrice().add(price));
        }
    }

    private void extractedOldOrder(Food food, Cart cart) {
        Optional<OrderItem> optional = cart
                .getOrderItems()
                .stream()
                .filter(ele -> ele.getFood().equals(food)).findFirst();
        if (optional.isPresent()) {
            OrderItem oldOrderItem = optional.get();
            cart.setPrice(cart.getPrice().subtract(oldOrderItem.getPrice()));
            cart.getOrderItems().remove(oldOrderItem);
        }
    }

    private OrderItem createOrderItem(Food food, int pieces, BigDecimal price) {
        OrderItem item = new OrderItem();
        item.setPrice(price);
        item.setFood(food);
        item.setPieces(pieces);
        return item;
    }

    private Cart getCart(Customer customer) {
        Cart cart = null;
        if (customer.getCart() == null) {
            cart = new Cart();
            customer.setCart(cart);
        } else {
            cart = customer.getCart();
        }
        return cart;
    }

    @Override
    public Order createOrder(Customer customer) {
        BigDecimal priceOfCart = customer.getCart().getPrice();
        BigDecimal balance = customer.getBalance();
        moneyVerification(priceOfCart, balance);

        Order order = new Order();
        order.setCustomerId(customer.getId());
        order.setPrice(customer.getCart().getPrice());
        order.setTimestampCreated(LocalDateTime.now());
        order.setOrderItems(customer.getCart().getOrderItems());
        order = dataStore.createOrder(order);

        customer.setBalance(balance.subtract(priceOfCart));
        customer.setCart(null);
        customer.setOrders(List.of(order));

        return order;
    }

    private void moneyVerification(BigDecimal priceOfCart, BigDecimal customersMoney) {
        if (priceOfCart.compareTo(BigDecimal.ZERO) == 0) {
            throw new IllegalStateException();
        }
        if (customersMoney.compareTo(priceOfCart) < 0) {
            throw new LowBalanceException(customersMoney.toString());
        }
    }
}