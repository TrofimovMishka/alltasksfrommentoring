package com.epam.training.game;

public class Bomberman {
    private static final char BOMB = 'O';
    private static final char EMPTY_PLACE = '.';

    public String[] run(int seconds, String[] starterGrid) {
        String[] fullOfBombs = createFullOfBombsArray(starterGrid);
        String[] gridAfterFirstExplosion = firstExplosion(starterGrid);
        String[] gridAfterSecondExplosion = secondExplosion(starterGrid, gridAfterFirstExplosion);

        if (seconds < 2) {
            return starterGrid;
        } else if (seconds % 2 == 0) {
            return fullOfBombs;
        } else if ((seconds - 1) % 4 == 0) {
            return gridAfterSecondExplosion;
        } else {
            return gridAfterFirstExplosion;
        }
    }

    private String[] firstExplosion(String[] starterGrid) {
        boolean[][] firstExplosionMatrix = createDefaultExplosionMatrix(starterGrid);
        bombsExplosion(starterGrid, firstExplosionMatrix);
        return createGridAfterExplosion(starterGrid, firstExplosionMatrix);
    }

    private String[] secondExplosion(String[] starterGrid, String[] gridAfterFirstExplosion) {
        boolean[][] secondExplosionMatrix = createDefaultExplosionMatrix(starterGrid);
        bombsExplosion(gridAfterFirstExplosion, secondExplosionMatrix);
        return createGridAfterExplosion(starterGrid, secondExplosionMatrix);
    }

    private void bombsExplosion(String[] grid, boolean[][] explosionMatrix) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length(); j++) {
                if (grid[i].charAt(j) == BOMB) {
                    bombRemoveCells(grid, explosionMatrix, i, j);
                }
            }
        }
    }

    private void bombRemoveCells(String[] grid, boolean[][] explosionMatrix, int i, int j) {
        explosionMatrix[i][j] = false;
        if (i > 0)
            explosionMatrix[i - 1][j] = false;
        if (i + 1 < grid.length)
            explosionMatrix[i + 1][j] = false;
        if (j > 0)
            explosionMatrix[i][j - 1] = false;
        if (j + 1 < grid[0].length())
            explosionMatrix[i][j + 1] = false;
    }

    private String[] createGridAfterExplosion(String[] grid, boolean[][] explosionMatrix) {
        String[] gridAfterExplosion = new String[grid.length];

        for (int i = 0; i < grid.length; i++) {
            gridAfterExplosion[i] = "";
            for (int j = 0; j < grid[0].length(); j++) {
                if (explosionMatrix[i][j]) {
                    gridAfterExplosion[i] += BOMB;
                } else {
                    gridAfterExplosion[i] += EMPTY_PLACE;
                }
            }
        }
        return gridAfterExplosion;
    }

    private boolean[][] createDefaultExplosionMatrix(String[] grid) {
        boolean[][] booleanMatrix = new boolean[grid.length][grid[0].length()];

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length(); j++) {
                booleanMatrix[i][j] = true;
            }
        }
        return booleanMatrix;
    }

    private String[] createFullOfBombsArray(String[] grid) {
        String[] fullOfBombs = new String[grid.length];

        for (int i = 0; i < grid.length; i++) {
            fullOfBombs[i] = "";
            for (int j = 0; j < grid[0].length(); j++) {
                fullOfBombs[i] += BOMB;
            }
        }
        return fullOfBombs;
    }
}
