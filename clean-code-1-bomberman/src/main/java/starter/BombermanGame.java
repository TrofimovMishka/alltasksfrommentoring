package starter;

import com.epam.training.game.Bomberman;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class BombermanGame {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        Bomberman bomberman = new Bomberman();
        System.out.println("Input numbers of rows, columns and seconds separated by space: ");
        String[] rowsColumnsSeconds = scanner.nextLine().split(" ");

        int numberOfRows = Integer.parseInt(rowsColumnsSeconds[0]);
        int numberOfColumns = Integer.parseInt(rowsColumnsSeconds[1]);
        int numberOfSeconds = Integer.parseInt(rowsColumnsSeconds[2]);

        String[] grid = fillUpGrid(numberOfRows, numberOfColumns);
        String[] result = bomberman.run(numberOfSeconds, grid);
        writeResultToFile(result);
    }

    private static String[] fillUpGrid(int numberOfRows, int numberOfColumns) {
        String[] grid = new String[numberOfRows];

        for (int i = 0; i < numberOfRows; i++) {
            System.out.printf("Input string should contain only letter 'O' and '.' and length must be %d characters: ", numberOfColumns);
            String gridItem = scanner.nextLine();

                while(isIncorrectString(gridItem, numberOfColumns)){
                    System.out.printf("Invalid Input. Input string should contain only letter 'O' and '.' and length must be %d characters: ", numberOfColumns);
                    gridItem = scanner.nextLine();
                }
            grid[i] = gridItem;
        }
        return grid;
    }

    private static boolean isIncorrectString(String inputFromUser, int numberOfCharacters){
        if (inputFromUser.length() > numberOfCharacters || inputFromUser.length() < numberOfCharacters){
            return false;
        }
        return  inputFromUser.matches("[^O.]");
    }

    private static void writeResultToFile(String[] result) throws IOException {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(".\\output.txt"))) {
            for (int i = 0; i < result.length; i++) {
                bufferedWriter.write(result[i]);
                if (i != result.length - 1) {
                    bufferedWriter.write("\n");
                }
            }
            bufferedWriter.newLine();
        }
    }
}
