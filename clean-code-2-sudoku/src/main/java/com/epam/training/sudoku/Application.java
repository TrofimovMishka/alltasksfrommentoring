package com.epam.training.sudoku;

import com.epam.training.sudoku.domain.Board;
import com.epam.training.sudoku.domain.Position;
import com.epam.training.sudoku.service.SudokuService;

import java.util.Scanner;

public class Application {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Board board = new Board(new int[][] {
            { 5, 3, 0, 0, 7, 0, 0, 0, 0 }, { 6, 0, 0, 1, 9, 5, 0, 0, 0 }, { 0, 9, 8, 0, 0, 0, 0, 6, 0 },
            { 8, 0, 0, 0, 6, 0, 0, 0, 3 }, { 4, 0, 0, 8, 0, 3, 0, 0, 1 }, { 7, 0, 0, 0, 2, 0, 0, 0, 6 },
            { 0, 6, 0, 0, 0, 0, 2, 8, 0 }, { 0, 0, 0, 4, 1, 9, 0, 0, 5 }, { 0, 0, 0, 0, 8, 0, 0, 7, 9 }
        });

        SudokuService sudokuService = new SudokuService(board);
        while (!sudokuService.isSolved()) {
            System.out.println(board);
            boolean isNotPlaceable = true;
            while (isNotPlaceable) {
                System.out.println("Please give row number(0-8): ");
                int row = Integer.parseInt(scanner.nextLine());
                System.out.println("Please give column number(0-8): ");
                int column = Integer.parseInt(scanner.nextLine());
                System.out.println("Please give number(1-9): ");
                int number = Integer.parseInt(scanner.nextLine());

                Position position = new Position(row, column);
                if (sudokuService.validate(position, number)) {
                    isNotPlaceable = false;
                    sudokuService.placeNumber(position, number);
                } else {
                    System.out.println("Not placeable. Please select another!");
                }
            }
        }
    }
}
