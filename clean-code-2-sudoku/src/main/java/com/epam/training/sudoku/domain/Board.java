package com.epam.training.sudoku.domain;

import java.util.ArrayList;
import java.util.List;

public class Board {
    private static final int VERTICAL_SIZE_OF_BOARD = 9;
    private static final int HORIZONTAL_SIZE_OF_BOARD = 9;
    public static final String MARKER_FOR_FIXED_VALUES = ". ";
    public static final String NO_MARKER = "  ";
    private final int[][] gameGrid;
    private List<Position> fixedPositions;

    public Board(int[][] gameGrid) {
        this.gameGrid = gameGrid;
        findAllFixedPosition();
    }

    public int[][] getGameSpace() {
        return gameGrid;
    }

    public int getVerticalSize() {
        return VERTICAL_SIZE_OF_BOARD;
    }

    public int getHorizontalSize() {
        return HORIZONTAL_SIZE_OF_BOARD;
    }

    public String toString() {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < VERTICAL_SIZE_OF_BOARD; i++) {
            for (int j = 0; j < HORIZONTAL_SIZE_OF_BOARD; j++) {
                if (isPositionFixed(new Position(i, j))) {
                    result.append(gameGrid[i][j]).append(MARKER_FOR_FIXED_VALUES);
                } else {
                    result.append(gameGrid[i][j]).append(NO_MARKER);
                }
            }
            result.append("\n");
        }
        return result.toString();
    }

    public boolean isPositionFixed(Position position) {
        return fixedPositions.contains(position);
    }

    public int getNumber(Position position) {
        return position.getNumber();
    }

    public void setNumber(Position position, int number) {
        int row = position.getRowIndex();
        int column = position.getColumnIndex();
        gameGrid[row][column] = number;
    }

    private void findAllFixedPosition() {
        fixedPositions = new ArrayList<>();
        for (int i = 0; i < VERTICAL_SIZE_OF_BOARD; i++) {
            for (int j = 0; j < HORIZONTAL_SIZE_OF_BOARD; j++) {
                if (gameGrid[i][j] > 0) {
                    fixedPositions.add(new Position(i, j));
                }
            }
        }
    }
}
