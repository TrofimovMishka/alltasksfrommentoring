package com.epam.training.sudoku.service;

import com.epam.training.sudoku.domain.Board;
import com.epam.training.sudoku.domain.Position;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class SudokuService {
    private final Board board;
    private static final int SIDE_OF_SQUARE = 3;
    private static final int VALID_COUNT_OF_NUMBERS = 8;
    private static final int MIN_INDEX_OF_POSITION = 0;
    private static final int MAX_INDEX_OF_POSITION = 8;
    private static final int MIN_NUMBER = 1;
    private static final int MAX_NUMBER = 9;

    public SudokuService(Board board) {
        this.board = board;
    }

    public boolean validate(Position position, int number) {
        return isPositionInABoardValid(position) && isNumberValid(number) && !board.isPositionFixed(position);
    }

    public void placeNumber(Position position, int number) {
        board.setNumber(position, number);
        position.setNumber(number);
    }

    public boolean isSolved() {
        if (isAllFieldsAreFilled()) {
            return isNumbersAreNotRepeatedInHorizontalLines()
                    && isNumbersAreNotRepeatedInVerticalLines()
                    && isNumbersAreNotRepeatedInSquares();
        }
        return false;
    }

    private boolean isPositionInABoardValid(Position position) {
        int column = position.getColumnIndex();
        int row = position.getRowIndex();

        return row >= MIN_INDEX_OF_POSITION && row <= MAX_INDEX_OF_POSITION
                && column >= MIN_INDEX_OF_POSITION && column <= MAX_INDEX_OF_POSITION;
    }

    private boolean isNumberValid(int number) {
        return number >= MIN_NUMBER && number <= MAX_NUMBER;
    }

    private boolean isNumbersAreNotRepeatedInSquares() {
        for (int i = 0; i <= SIDE_OF_SQUARE * 2; i += SIDE_OF_SQUARE) {
            for (int j = 0; j <= SIDE_OF_SQUARE * 2; j += SIDE_OF_SQUARE) {
                if (isNumbersAreRepeated(i, j)) return false;
            }
        }
        return true;
    }

    private boolean isNumbersAreRepeated(int horizontalPointBeginOfTheSquare, int verticalPointBeginOfTheSquare) {
        Set<Integer> numbersInOneSquare = new HashSet<>();

        for (int i = horizontalPointBeginOfTheSquare; i < horizontalPointBeginOfTheSquare + SIDE_OF_SQUARE; i++) {
            for (int j = verticalPointBeginOfTheSquare; j < verticalPointBeginOfTheSquare + SIDE_OF_SQUARE; j++) {
                numbersInOneSquare.add(board.getGameSpace()[i][j]);
            }
        }
        return numbersInOneSquare.size() < VALID_COUNT_OF_NUMBERS;
    }

    private boolean isNumbersAreNotRepeatedInHorizontalLines() {
        for (int i = 0; i < board.getHorizontalSize(); i++) {
            Set<Integer> numbersInHorizontalLine = Arrays.stream(board.getGameSpace()[i])
                    .boxed()
                    .collect(Collectors.toSet());
            if (numbersInHorizontalLine.size() < VALID_COUNT_OF_NUMBERS) {
                return false;
            }
        }
        return true;
    }

    private boolean isNumbersAreNotRepeatedInVerticalLines() {
        for (int i = 0; i < board.getVerticalSize(); i++) {
            Set<Integer> numbersInVerticalLine = new HashSet<>();
            for (int j = 0; j < board.getHorizontalSize(); j++) {
                numbersInVerticalLine.add(board.getGameSpace()[j][i]);
            }
            if (numbersInVerticalLine.size() < VALID_COUNT_OF_NUMBERS) {
                return false;
            }
        }
        return true;
    }

    private boolean isAllFieldsAreFilled() {
        for (int i = 0; i < board.getVerticalSize(); i++) {
            int minValueInArray = Arrays.stream(board.getGameSpace()[i]).min().orElseThrow();
            if (minValueInArray == 0) { return false; }
        }
        return true;
    }
}
