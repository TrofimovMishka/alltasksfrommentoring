package com.epam.training.modeltransformer.projection.rotation;

public class Matrix {
    private final Angle angle;

    public Matrix(Angle angle) {
        this.angle = angle;
    }

    public double[][] getXMatrix() {
        double x = angle.getAngleX();
        return new double[][]{
                {1, 0, 0},
                {0, Math.cos(x), Math.sin(-x)},
                {0, Math.sin(x), Math.cos(x)}
        };
    }

    public double[][] getYMatrix() {
        double y = angle.getAngleY();
        return new double[][]{
                {Math.cos(y), 0, Math.sin(y)},
                {0, 1, 0},
                {Math.sin(-y), 0, Math.cos(y)}
        };
    }

    public double[][] getZMatrix() {
        double z = angle.getAngleZ();
        return new double[][]{
                {Math.cos(z), Math.sin(-z), 0},
                {Math.sin(z), Math.cos(z), 0},
                {0, 0, 1}
        };
    }
}
