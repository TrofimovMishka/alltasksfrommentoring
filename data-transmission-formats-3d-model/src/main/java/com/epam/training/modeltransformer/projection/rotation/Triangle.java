package com.epam.training.modeltransformer.projection.rotation;

public class Triangle {
    private final Point aPointCoordinate;
    private final Point bPointCoordinate;
    private final Point cPointCoordinate;
    private double averageZ;
    private double minXValue;
    private double minYValue;

    public Triangle(Point aPointCoordinate, Point bPointCoordinate, Point cPointCoordinate) {
        this.aPointCoordinate = aPointCoordinate;
        this.bPointCoordinate = bPointCoordinate;
        this.cPointCoordinate = cPointCoordinate;

        calculateAverageZ();
        findMinValues();
    }

    public void calculateAverageZ() {
        averageZ = (aPointCoordinate.getZCoordinate()
                + bPointCoordinate.getZCoordinate()
                + cPointCoordinate.getZCoordinate()) / 3;
    }

    public double getAverageZ() {
        return averageZ;
    }

    public Point getAPointCoordinate() {
        return aPointCoordinate;
    }

    public Point getBPointCoordinate() {
        return bPointCoordinate;
    }

    public Point getCPointCoordinate() {
        return cPointCoordinate;
    }

    public void findMinValues() {
        minXValue = Math.min
                (Math.min(aPointCoordinate.getXCoordinate(), bPointCoordinate.getXCoordinate())
                        , cPointCoordinate.getXCoordinate());
        minYValue = Math.min
                (Math.min(aPointCoordinate.getYCoordinate(), bPointCoordinate.getYCoordinate())
                        , cPointCoordinate.getYCoordinate());
    }

    public double getMinXValue() {
        return minXValue;
    }

    public double getMinYValue() {
        return minYValue;
    }

    public void changeXValue(double value) {
        aPointCoordinate.setXCoordinate(aPointCoordinate.getXCoordinate() + Math.abs(value));
        bPointCoordinate.setXCoordinate(bPointCoordinate.getXCoordinate() + Math.abs(value));
        cPointCoordinate.setXCoordinate(cPointCoordinate.getXCoordinate() + Math.abs(value));
    }

    public void changeYValue(double value) {
        aPointCoordinate.setYCoordinate(aPointCoordinate.getYCoordinate() + Math.abs(value));
        bPointCoordinate.setYCoordinate(bPointCoordinate.getYCoordinate() + Math.abs(value));
        cPointCoordinate.setYCoordinate(cPointCoordinate.getYCoordinate() + Math.abs(value));
    }
}
