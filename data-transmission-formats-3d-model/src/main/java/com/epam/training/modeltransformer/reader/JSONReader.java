package com.epam.training.modeltransformer.reader;

import com.epam.training.modeltransformer.projection.rotation.Angle;
import com.epam.training.modeltransformer.projection.rotation.Matrix;
import com.epam.training.modeltransformer.projection.rotation.Point;
import com.epam.training.modeltransformer.projection.rotation.Triangle;
import jakarta.json.Json;
import jakarta.json.stream.JsonParser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class JSONReader {
    private final String inputFilePath;
    private JsonParser parser;
    private List<Triangle> triangles;

    public JSONReader(String inputFilePath) {
        this.inputFilePath = inputFilePath;
    }

    public List<Triangle> getTriangles(Angle angle) throws FileNotFoundException {
        triangles = new ArrayList<>();
        parser = Json.createParser(new FileInputStream(inputFilePath));

        while (parser.hasNext()) {
            JsonParser.Event event = parser.next();
            if (event == JsonParser.Event.START_OBJECT) {
                createTriangle(angle);
            }
        }
        return triangles;
    }

    private void createTriangle(Angle angle) {
        Point aPointCoordinate = rotate(createPoint(parser), angle);
        Point bPointCoordinate = rotate(createPoint(parser), angle);
        Point cPointCoordinate = rotate(createPoint(parser), angle);

        triangles.add(new Triangle(aPointCoordinate, bPointCoordinate, cPointCoordinate));
    }

    private Point createPoint(JsonParser parser) {
        JsonParser.Event event;
        Point point = new Point();
        while ((event = parser.next()) != JsonParser.Event.END_OBJECT) {
            if (event == JsonParser.Event.KEY_NAME) {
                fillUpCoordinates(parser, point);
            }
        }
        return point;
    }

    private void fillUpCoordinates(JsonParser parser, Point point) {
        if (parser.getString().equals("x")) {
            parser.next();
            point.setXCoordinate(parser.getInt());
        } else if (parser.getString().equals("y")) {
            parser.next();
            point.setYCoordinate(parser.getInt());
        } else if (parser.getString().equals("z")) {
            parser.next();
            point.setZCoordinate(parser.getInt());
        }
    }

    private Point rotate(Point point, Angle angle) {
        Matrix matrix = new Matrix(angle);
        double[][] pointMatrix = {{point.getXCoordinate()}, {point.getYCoordinate()}, {point.getZCoordinate()}};

        double[][] rotateByX = multiplyMatrices(matrix.getXMatrix(), pointMatrix);
        double[][] rotateByY = multiplyMatrices(matrix.getYMatrix(), rotateByX);
        double[][] rotateByZ = multiplyMatrices(matrix.getZMatrix(), rotateByY);

        point.setXCoordinate(rotateByZ[0][0]);
        point.setYCoordinate(rotateByZ[1][0]);
        point.setZCoordinate(rotateByZ[2][0]);

        return point;
    }

    private double[][] multiplyMatrices(double[][] angleMatrix, double[][] pointMatrix) {
        double[][] result = new double[angleMatrix.length][pointMatrix[0].length];

        for (int i = 0; i < angleMatrix.length; i++) {
            for (int j = 0; j < pointMatrix[i].length; j++) {
                for (int k = 0; k < pointMatrix.length; k++) {
                    result[i][j] += (angleMatrix[i][k] * pointMatrix[k][j]);
                }
            }
        }
        return result;
    }
}
