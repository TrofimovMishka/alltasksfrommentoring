package com.epam.training.modeltransformer.transformer;

import com.epam.training.modeltransformer.projection.rotation.Angle;
import com.epam.training.modeltransformer.projection.rotation.Triangle;
import com.epam.training.modeltransformer.reader.JSONReader;
import com.epam.training.modeltransformer.writer.XMLWriter;

import java.util.List;

public class TriangleTransformer {
    List<Triangle> triangles;

    public void transform(String inputFilePath, String outputFilePath, Angle angle) {
        try {
            JSONReader jsonReader = new JSONReader(inputFilePath);
            triangles = jsonReader.getTriangles(angle);

            offsetCoordinates();
            sortTriangles();

            XMLWriter xmlWriter = new XMLWriter(outputFilePath);
            xmlWriter.writeObjects(triangles);

        } catch (Exception ignore) {
        }
    }

    private void sortTriangles() {
        triangles.sort((o1, o2) -> Double.compare(o2.getAverageZ(), o1.getAverageZ()));
    }

    private void offsetCoordinates() {
        double[] minValues = findMinValues();
        if (minValues[0] < 0) {
            triangles.forEach(triangle -> triangle.changeXValue(minValues[0]));
        }
        if (minValues[1] < 0) {
            triangles.forEach(triangle -> triangle.changeYValue(minValues[1]));
        }
    }

    // change for use steam and use map for return values
    private double[] findMinValues() {
        double minX = 0;
        double minY = 0;
        for (Triangle triangle :
                triangles) {
            minX = Math.min(minX, triangle.getMinXValue());
            minY = Math.min(minY, triangle.getMinYValue());
        }
        return new double[]{minX, minY};
    }
}
