package com.epam.training.modeltransformer.writer;

import com.epam.training.modeltransformer.projection.rotation.Triangle;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class XMLWriter {
    private final XMLStreamWriter writer;

    public XMLWriter(String outputFilePath) throws IOException, XMLStreamException {
        XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
        FileWriter fileWriter = new FileWriter(outputFilePath);
        this.writer = xmlOutputFactory.createXMLStreamWriter(fileWriter);
    }

    public void writeObjects(List<Triangle> triangles) throws XMLStreamException {
        writeStartParts();
        for (Triangle triangle : triangles) {
            writer.writeStartElement("polygon");
            String value = getValue(triangle);
            writer.writeAttribute("points", value);
            writer.writeAttribute("style", "fill:white;stroke:black;stroke-width:0.1");
            writer.writeEndElement();
        }
        writeEndParts();
    }

    private void writeStartParts() throws XMLStreamException {
        writer.writeStartDocument();
        writer.writeStartElement("html");
        writer.writeStartElement("body");
        writer.writeStartElement("svg");
        writer.writeAttribute("width", "1000");
        writer.writeAttribute("height", "1000");
    }

    private void writeEndParts() throws XMLStreamException {
        writer.writeEndElement();
        writer.writeEndElement();
        writer.writeEndDocument();
        writer.close();
    }
//Think about name or create toString in triangle class
    // Create new class for create string
    //use strategy pattern
    private String getValue(Triangle triangle) {
        StringBuilder sb = new StringBuilder();
        sb.append(triangle.getAPointCoordinate().getXCoordinate()).append(",");
        sb.append(triangle.getAPointCoordinate().getYCoordinate()).append(" ");
        sb.append(triangle.getBPointCoordinate().getXCoordinate()).append(",");
        sb.append(triangle.getBPointCoordinate().getYCoordinate()).append(" ");
        sb.append(triangle.getCPointCoordinate().getXCoordinate()).append(",");
        sb.append(triangle.getCPointCoordinate().getYCoordinate());
        return sb.toString();
    }
}
