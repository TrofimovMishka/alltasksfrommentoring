package com.epam.training.smarthome;

import com.epam.training.smarthome.commands.EventCommandFactory;
import com.epam.training.smarthome.commands.EventCommandType;
import com.epam.training.smarthome.controller.HomeController;
import com.epam.training.smarthome.domain.devices.AlarmSystem;
import com.epam.training.smarthome.domain.devices.Light;
import com.epam.training.smarthome.domain.observer.MessageObserver;
import com.epam.training.smarthome.domain.observer.Observer;

class Application {
    public static void main(String[] args) {
        Observer observer = new MessageObserver();

        HomeController controller = new HomeController.HomeControllerBuilder(observer)
                .light(new Light(true))
                .alarmSystem(new AlarmSystem(true))
                .build();

        EventCommandFactory factory = new EventCommandFactory(controller);
        factory.createEventCommand(EventCommandType.CHANGE_TO_HOLIDAY).execute();
        factory.createEventCommand(EventCommandType.GOING_HOME).execute();
        factory.createEventCommand(EventCommandType.MOVEMENT).execute();
        factory.createEventCommand(EventCommandType.ARRIVES_HOME).execute();
        factory.createEventCommand(EventCommandType.MOVEMENT).execute();
        factory.createEventCommand(EventCommandType.CHANGE_TO_WORKING_DAY).execute();
        factory.createEventCommand(EventCommandType.GOING_HOME).execute();
        factory.createEventCommand(EventCommandType.ARRIVES_HOME).execute();
        factory.createEventCommand(EventCommandType.MOVEMENT).execute();

        System.out.println("\nAll messages dispatched by the devices:");
        observer.getMessages().forEach(System.out::println);
    }
}
