package com.epam.training.smarthome.commands;

import com.epam.training.smarthome.controller.HomeController;

public class ArrivesHome extends EventCommand {
    public ArrivesHome(HomeController homeController) {
        super(homeController);
    }

    @Override
    public void execute() {
        homeController.onArriveHome();
    }
}
