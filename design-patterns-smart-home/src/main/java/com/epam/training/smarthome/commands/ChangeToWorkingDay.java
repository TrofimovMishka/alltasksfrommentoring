package com.epam.training.smarthome.commands;

import com.epam.training.smarthome.controller.HomeController;

public class ChangeToWorkingDay extends EventCommand {
    public ChangeToWorkingDay(HomeController homeController) {
        super(homeController);
    }

    @Override
    public void execute() {
        homeController.onChangeToWorkingDay();
    }
}
