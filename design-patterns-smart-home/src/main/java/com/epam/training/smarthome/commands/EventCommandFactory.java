package com.epam.training.smarthome.commands;

import com.epam.training.smarthome.controller.HomeController;

public class EventCommandFactory {
    private HomeController homeController;

    public EventCommandFactory(HomeController homeController) {
        this.homeController = homeController;
    }

    public EventCommand createEventCommand(EventCommandType type) {
        return switch (type) {
            case ARRIVES_HOME -> new ArrivesHome(homeController);
            case GOING_HOME -> new GoingHome(homeController);
            case MOVEMENT -> new Movement(homeController);
            case CHANGE_TO_HOLIDAY -> new ChangeToHoliday(homeController);
            case CHANGE_TO_WORKING_DAY -> new ChangeToWorkingDay(homeController);
        };
    }
}
