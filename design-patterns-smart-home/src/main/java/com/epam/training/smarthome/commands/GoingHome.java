package com.epam.training.smarthome.commands;

import com.epam.training.smarthome.controller.HomeController;

public class GoingHome extends EventCommand {
    public GoingHome(HomeController homeController) {
        super(homeController);
    }

    @Override
    public void execute() {
        homeController.onGoingHome();
    }
}
