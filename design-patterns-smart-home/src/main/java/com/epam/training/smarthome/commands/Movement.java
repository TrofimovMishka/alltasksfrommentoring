package com.epam.training.smarthome.commands;

import com.epam.training.smarthome.controller.HomeController;

public class Movement extends EventCommand {
    public Movement(HomeController homeController) {
        super(homeController);
    }

    @Override
    public void execute() {
        homeController.onMovement();
    }
}
