package com.epam.training.smarthome.controller;

import com.epam.training.smarthome.domain.devices.AlarmSystem;
import com.epam.training.smarthome.domain.devices.FrontDoor;
import com.epam.training.smarthome.domain.devices.Light;
import com.epam.training.smarthome.domain.devices.coffeemaker.CoffeeMaker;
import com.epam.training.smarthome.domain.devices.coffeemaker.StrongCoffeeCreationStrategy;
import com.epam.training.smarthome.domain.devices.coffeemaker.WeakCoffeeCreationStrategy;
import com.epam.training.smarthome.domain.devices.heatingsystem.HeatingSystem;
import com.epam.training.smarthome.domain.devices.heatingsystem.HeatingSystemAdapter;
import com.epam.training.smarthome.domain.devices.heatingsystem.LegacyHeatingSystem;
import com.epam.training.smarthome.domain.observer.Observer;

public class HomeController {
    private AlarmSystem alarmSystem;
    private FrontDoor frontDoor;
    private HeatingSystemAdapter heatingSystemAdapter;
    private Light light;
    private CoffeeMaker coffeeMaker;

    public void onMovement() {
        System.out.println("\n--> Movement event");
        if (alarmSystem.isTurnedOn()) {
            alarmSystem.alarm();
        }
        if (light.isTurnedOn()) {
            System.out.println("[HomeController] nothing to do (light is already turned on)");
        } else {
            light.turnOn();
        }
    }

    public void onGoingHome() {
        System.out.println("\n--> Going home event");
        if (heatingSystemAdapter.isTurnedOn()) {
            System.out.println("[HomeController] nothing to do (heating system is already turned on)");
        } else {
            heatingSystemAdapter.turnOn();
        }
    }

    public void onArriveHome() {
        System.out.println("\n--> Arrive home event");
        if (alarmSystem.isTurnedOn()) {
            alarmSystem.turnOff();
        } else {
            System.out.println("[HomeController] nothing to do (alarm system is already turned off)");
        }

        if (frontDoor.isOpen()) {
            System.out.println("[HomeController] nothing to do (front door is already open)");
        } else {
            frontDoor.open();
        }
        coffeeMaker.createCoffee();
    }

    public void onChangeToHoliday() {
        System.out.println("\n--> Change to holiday event");
        coffeeMaker.setCoffeeCreationStrategy(new WeakCoffeeCreationStrategy());
    }

    public void onChangeToWorkingDay() {
        System.out.println("\n--> Change to working day event");
        coffeeMaker.setCoffeeCreationStrategy(new StrongCoffeeCreationStrategy());
    }

    public void setAlarmSystem(AlarmSystem alarmSystem) {
        this.alarmSystem = alarmSystem;
    }

    public void setFrontDoor(FrontDoor frontDoor) {
        this.frontDoor = frontDoor;
    }

    public void setHeatingSystemAdapter(HeatingSystemAdapter heatingSystemAdapter) {
        this.heatingSystemAdapter = heatingSystemAdapter;
    }

    public void setLight(Light light) {
        this.light = light;
    }

    public void setCoffeeMaker(CoffeeMaker coffeeMaker) {
        this.coffeeMaker = coffeeMaker;
    }

    public static class HomeControllerBuilder {
        private Observer messageObserver;
        private AlarmSystem alarmSystem = new AlarmSystem(false);
        private FrontDoor frontDoor = new FrontDoor(false);
        private HeatingSystem heatingSystem = new HeatingSystem(new LegacyHeatingSystem(false));
        private Light light = new Light(false);
        private CoffeeMaker coffeeMaker = new CoffeeMaker(new StrongCoffeeCreationStrategy());

        public HomeControllerBuilder(Observer messageObserver) {
            this.messageObserver = messageObserver;
        }

        public HomeControllerBuilder alarmSystem(AlarmSystem alarmSystem) {
            this.alarmSystem = alarmSystem;
            return this;
        }

        public HomeControllerBuilder frontDoor(FrontDoor frontDoor) {
            this.frontDoor = frontDoor;
            return this;
        }

        public HomeControllerBuilder heatingSystem(HeatingSystem heatingSystem) {
            this.heatingSystem = heatingSystem;
            return this;
        }

        public HomeControllerBuilder light(Light light) {
            this.light = light;
            return this;
        }

        public HomeControllerBuilder coffeeMaker(CoffeeMaker coffeeMaker) {
            this.coffeeMaker = coffeeMaker;
            return this;
        }

        public HomeController build() {
            HomeController controller = new HomeController();
            addObserverToDevices();
            setDevicesToController(controller);

            return controller;
        }

        private void setDevicesToController(HomeController controller) {
            controller.setHeatingSystemAdapter(heatingSystem);
            controller.setAlarmSystem(alarmSystem);
            controller.setCoffeeMaker(coffeeMaker);
            controller.setFrontDoor(frontDoor);
            controller.setLight(light);
        }

        private void addObserverToDevices() {
            heatingSystem.addObserver(messageObserver);
            alarmSystem.addObserver(messageObserver);
            coffeeMaker.addObserver(messageObserver);
            frontDoor.addObserver(messageObserver);
            light.addObserver(messageObserver);
        }
    }
}
