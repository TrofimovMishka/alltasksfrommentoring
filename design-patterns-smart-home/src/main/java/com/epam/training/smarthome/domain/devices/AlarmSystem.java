package com.epam.training.smarthome.domain.devices;

import com.epam.training.smarthome.domain.observer.Observable;

public class AlarmSystem extends Observable {
    private boolean isTurnedOn;

    public AlarmSystem(boolean isTurnedOn) {
        this.isTurnedOn = isTurnedOn;
    }

    public void alarm() {
        String message = "[AlarmSystem] alarm";
        notifyObservers(message);
        System.out.println(message);
    }

    public boolean isTurnedOn() {
        return isTurnedOn;
    }

    public void turnOff() {
        isTurnedOn = false;
        String message = "[AlarmSystem] turn off";
        System.out.println(message);
        notifyObservers(message);
    }

    public void turnOn() {
        isTurnedOn = true;
        String message = "[AlarmSystem] turn on";
        System.out.println(message);
        notifyObservers(message);
    }

}
