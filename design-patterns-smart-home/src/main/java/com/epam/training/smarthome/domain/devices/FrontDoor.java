package com.epam.training.smarthome.domain.devices;

import com.epam.training.smarthome.domain.observer.Observable;

public class FrontDoor extends Observable {
    private boolean isOpen;

    public FrontDoor(boolean isOpen) {
        this.isOpen = isOpen;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void open() {
        String message = "[FrontDoor] open";
        System.out.println(message);
        notifyObservers(message);
        isOpen = true;
    }

    public void close() {
        isOpen = false;
        String message = "[FrontDoor] close";
        System.out.println(message);
        notifyObservers(message);
    }

}
