package com.epam.training.smarthome.domain.devices;

import com.epam.training.smarthome.domain.observer.Observable;

public class Light extends Observable {
    private boolean isTurnedOn;

    public Light(boolean isTurnedOn) {
        this.isTurnedOn = isTurnedOn;
    }

    public boolean isTurnedOn() {
        return isTurnedOn;
    }

    public void turnOff() {
        isTurnedOn = false;
        String message = "[Light] turn off";
        System.out.println(message);
        notifyObservers(message);
    }

    public void turnOn() {
        isTurnedOn = true;
        String message = "[Light] turn on";
        System.out.println(message);
        notifyObservers(message);
    }

}
