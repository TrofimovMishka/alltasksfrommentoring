package com.epam.training.smarthome.domain.devices.coffeemaker;

import com.epam.training.smarthome.domain.observer.Observable;

public class CoffeeMaker extends Observable {
    private CoffeeCreationStrategy coffeeCreationStrategy;

    public CoffeeMaker(CoffeeCreationStrategy coffeeCreationStrategy) {
        this.coffeeCreationStrategy = coffeeCreationStrategy;
    }

    public void setCoffeeCreationStrategy(CoffeeCreationStrategy coffeeCreationStrategy) {
        String message = "[CoffeeMaker] change the type of coffee";
        System.out.println(message);
        this.coffeeCreationStrategy = coffeeCreationStrategy;
        notifyObservers(message);
    }

    public void createCoffee() {
        Integer howCoffee = coffeeCreationStrategy.getCaffeineInMg();
        String message = "[CoffeeMaker] create coffee with " + howCoffee + "mg caffeine";
        System.out.println(message);
        notifyObservers(message);
    }

}
