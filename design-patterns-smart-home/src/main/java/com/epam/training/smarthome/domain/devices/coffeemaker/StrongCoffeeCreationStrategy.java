package com.epam.training.smarthome.domain.devices.coffeemaker;

public class StrongCoffeeCreationStrategy implements CoffeeCreationStrategy {
    public static final Integer CAFFEINE_IN_MG = 40;

    @Override
    public Integer getCaffeineInMg() {
        return CAFFEINE_IN_MG;
    }
}
