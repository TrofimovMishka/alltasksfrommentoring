package com.epam.training.smarthome.domain.devices.coffeemaker;

public class WeakCoffeeCreationStrategy implements CoffeeCreationStrategy {
    public static final Integer CAFFEINE_IN_MG = 20;

    @Override
    public Integer getCaffeineInMg() {
        return CAFFEINE_IN_MG;
    }
}
