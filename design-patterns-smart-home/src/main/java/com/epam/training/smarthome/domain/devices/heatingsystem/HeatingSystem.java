package com.epam.training.smarthome.domain.devices.heatingsystem;

import com.epam.training.smarthome.domain.observer.Observable;

public class HeatingSystem extends Observable implements HeatingSystemAdapter {
    private LegacyHeatingSystem legacyHeatingSystem;

    public HeatingSystem(LegacyHeatingSystem legacyHeatingSystem) {
        this.legacyHeatingSystem = legacyHeatingSystem;
    }

    @Override
    public void turnOff() {
        legacyHeatingSystem.operate(false);
        String message = "[HeatingSystem] turn off";
        System.out.println(message);
        notifyObservers(message);
    }

    @Override
    public void turnOn() {
        legacyHeatingSystem.operate(true);
        String message = "[HeatingSystem] turn on";
        System.out.println(message);
        notifyObservers(message);
    }

    @Override
    public boolean isTurnedOn() {
        return legacyHeatingSystem.isTurnedOn();
    }

}
