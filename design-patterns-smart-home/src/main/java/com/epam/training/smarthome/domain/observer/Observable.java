package com.epam.training.smarthome.domain.observer;

import java.util.ArrayList;
import java.util.List;

public class Observable {
    private List<Observer> observers;

    public void addObserver(Observer observer) {
        if (observers == null) {
            observers = new ArrayList<>();
        }
        observers.add(observer);
    }

    public void notifyObservers(String message) {
        observers.forEach(observer -> observer.update(message));
    }
}
