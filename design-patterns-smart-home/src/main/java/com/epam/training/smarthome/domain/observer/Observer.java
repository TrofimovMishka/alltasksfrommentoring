package com.epam.training.smarthome.domain.observer;

import java.util.List;

public interface Observer {
    void update(String message);

    List<String> getMessages();
}
