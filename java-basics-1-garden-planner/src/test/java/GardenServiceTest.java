import com.epam.training.garden.domain.GardenProperties;
import com.epam.training.garden.service.GardenService;
import com.epam.training.garden.service.Result;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Map;

class GardenServiceTest {
    private static GardenService gardenService;

    @BeforeAll
    static void setUp() {
        gardenService = new GardenService();
        gardenService.setGardenProperties(new GardenProperties(20, 170));
    }

    @Test
    void testEnoughAreaAndWaterEvaluatePlan() {
        Map<String, Integer> testData = Map.of("Corn", 10, "Tomato", 10);

        Result expected = new Result(7, 70, true, true);
        Result actual = gardenService.evaluatePlan(testData);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testUnknownPlantEvaluatePlan() {
        Map<String, Integer> testData = Map.of("Corn", 10, "Tomato", 10, "Onion", 5);

        Assertions.assertThrows(IllegalArgumentException.class, () -> gardenService.evaluatePlan(testData));
    }

    @Test
    void testNotEnoughAreaAndWaterEvaluatePlan() {
        Map<String, Integer> testData = Map.of("Grape", 10, "Tomato", 10);

        Result expected = new Result(33, 180, false, false);
        Result actual = gardenService.evaluatePlan(testData);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    void testNotEnoughAreaEvaluatePlan() {
        Map<String, Integer> testData = Map.of("Grape", 5, "Pumpkin", 4);

        Result expected = new Result(23, 115, false, true);
        Result actual = gardenService.evaluatePlan(testData);

        Assertions.assertEquals(expected, actual);
    }

}