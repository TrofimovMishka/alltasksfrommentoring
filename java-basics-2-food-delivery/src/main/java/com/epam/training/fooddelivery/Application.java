package com.epam.training.fooddelivery;

import com.epam.training.fooddelivery.data.FileBasedDataStore;
import com.epam.training.fooddelivery.service.FoodDeliveryService;
import com.epam.training.fooddelivery.view.View;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import java.util.NoSuchElementException;

public class Application {
    static {
        Locale.setDefault(Locale.ENGLISH);
    }

    public static void main(String[] args) throws IOException {

        FileBasedDataStore fileBased = new FileBasedDataStore("C:\\Users\\Mike\\IdeaProjects\\epamTasks" +
                "\\java-basics-2-food-delivery\\input\\orders.csv");
        View view = new View();
        view.printWelcomeMessage();

        FoodDeliveryService service = new FoodDeliveryService(fileBased);

        view.printMostExpensiveOrder(service.getMostExpensiveOrder());
        view.printMostPopularFood(service.getMostPopularFood());
        view.printMostLoyalCustomer(service.getMostLoyalCustomerId());

        LocalDate startDate = view.readStartDate();
        LocalDate endDate = view.readEndDate();

        try {
            view.printStatistics(service.getStatistics(startDate, endDate), startDate, endDate);
        } catch (NoSuchElementException ex) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            String dateStart = startDate.format(formatter);
            String dateEnd = endDate.format(formatter);
            System.out.println("\nNo orders were found between " + dateStart + " and " + dateEnd);
        }
    }
}
