package com.epam.training.fooddelivery.data;

import com.epam.training.fooddelivery.domain.Food;
import com.epam.training.fooddelivery.domain.Order;
import com.epam.training.fooddelivery.domain.OrderItem;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class FileBasedDataStore implements DataStore {

    private List<Order> orders;
    private String pathOfFile;
    private static Map<String, BigDecimal> foodAndPrice;
    private List<String> infoFromFile = new ArrayList<>();

    static {
        foodAndPrice = new HashMap<>();
        foodAndPrice.put("Fideua", BigDecimal.valueOf(15));
        foodAndPrice.put("Paella", BigDecimal.valueOf(13));
        foodAndPrice.put("Tortilla", BigDecimal.valueOf(10));
        foodAndPrice.put("Gazpacho", BigDecimal.valueOf(8));
        foodAndPrice.put("Quesadilla", BigDecimal.valueOf(13));
    }

    public FileBasedDataStore(String pathOfFile) {
        this.pathOfFile = pathOfFile;
    }

    @Override
    public List<Order> getOrders() {
        if (orders == null) {
            orders = new ArrayList<>();
            try (BufferedReader reader =
                         new BufferedReader(new FileReader(pathOfFile))) {
                String line = "";
                while ((line = reader.readLine()) != null) {
                    String[] stringsFromLine = line.split(",");
                    if (stringsFromLine.length < 5) {
                        throw new IllegalArgumentException();
                    }
                    Order order = new Order();
                    order.setId(Integer.parseInt(stringsFromLine[0]));
                    order.setCustomerId(Long.parseLong(stringsFromLine[1]));
                    String orderTime = stringsFromLine[2];
                    String foodName = stringsFromLine[3];
                    int amount = Integer.parseInt(stringsFromLine[4]);

                    BigDecimal totalPrice =
                            foodAndPrice.get(foodName).multiply(BigDecimal.valueOf(amount));
                    OrderItem orderItem = getOrderItem(foodName, amount, totalPrice);
                    LocalDateTime dateTime = parseTime(orderTime);

                    Order newOrder = getActualOrder(order);
                    if (newOrder != null) {
                        order = newOrder;
                        totalPrice = totalPrice.add(order.getTotalPrice());
                    }
                    order.setOrderDate(dateTime);
                    order.setOrderItems(List.of(orderItem));
                    order.setTotalPrice(totalPrice);

                    if (newOrder == null) {
                        orders.add(order);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return orders;
    }

    private Order getActualOrder(Order order) {
        Optional<Order> optional = orders.stream()
                .filter(ele -> ele.getId() == order.getId())
                .findFirst();
        if (optional.isEmpty()) {
            return null;
        }
        return optional.get();
    }

    private LocalDateTime parseTime(String time) {
        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime dateTime = LocalDateTime.parse(time, formatter);
        return dateTime;
    }

    private OrderItem getOrderItem(String foodName, int amount, BigDecimal totalPrice) {
        Food food = new Food(foodName, foodAndPrice.get(foodName));
        OrderItem orderItem = new OrderItem(food, amount, totalPrice);
        return orderItem;
    }
}
