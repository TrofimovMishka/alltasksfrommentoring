package com.epam.training.fooddelivery.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class Statistics {
    private BigDecimal totalIncome;
    private double averageIncomeByOrder;
    private int numberOfFood;
    private int numberOfOrder;
    private LocalDate dayOfHighestIncome;

    public Statistics() {
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public double getAverageIncomeByOrder() {
        return averageIncomeByOrder;
    }

    public void setAverageIncomeByOrder(double averageIncomeByOrder) {
        this.averageIncomeByOrder = averageIncomeByOrder;
    }

    public int getNumberOfFood() {
        return numberOfFood;
    }

    public void setNumberOfFood(int numberOfFood) {
        this.numberOfFood = numberOfFood;
    }

    public int getNumberOfOrder() {
        return numberOfOrder;
    }

    public void setNumberOfOrder(int numberOfOrder) {
        this.numberOfOrder = numberOfOrder;
    }

    public LocalDate getDayOfHighestIncome() {
        return dayOfHighestIncome;
    }

    public void setDayOfHighestIncome(LocalDate dayOfHighestIncome) {
        this.dayOfHighestIncome = dayOfHighestIncome;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "totalIncome=" + totalIncome +
                ", averageIncomeByOrder=" + String.format("%.2f", averageIncomeByOrder) +
                ", numberOfFood=" + numberOfFood +
                ", numberOfOrder=" + numberOfOrder +
                ", dayOfHighestIncome=" + dayOfHighestIncome +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Statistics that = (Statistics) o;
        return Double.compare(that.averageIncomeByOrder, averageIncomeByOrder) == 0
                && numberOfFood == that.numberOfFood && numberOfOrder == that.numberOfOrder
                && Objects.equals(totalIncome, that.totalIncome)
                && Objects.equals(dayOfHighestIncome, that.dayOfHighestIncome);
    }

    @Override
    public int hashCode() {
        return Objects.hash(totalIncome, averageIncomeByOrder, numberOfFood, numberOfOrder, dayOfHighestIncome);
    }
}
