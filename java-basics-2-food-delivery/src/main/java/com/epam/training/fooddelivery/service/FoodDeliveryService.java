package com.epam.training.fooddelivery.service;

import com.epam.training.fooddelivery.data.DataStore;
import com.epam.training.fooddelivery.domain.Food;
import com.epam.training.fooddelivery.domain.Order;
import com.epam.training.fooddelivery.domain.OrderItem;
import com.epam.training.fooddelivery.domain.Statistics;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class FoodDeliveryService {
    private DataStore dataStore;

    public FoodDeliveryService(DataStore dataStore) {
        this.dataStore = dataStore;
    }

    public List<Order> getOrders(LocalDate startDate, LocalDate endDate) {
        List<Order> ordersBetweenDates = null;
        ordersBetweenDates = dataStore
                .getOrders()
                .stream()
                .filter(order ->
                {
                    LocalDate orderDate = order.getOrderDate().toLocalDate();
                    if (orderDate.isAfter(startDate)
                            && orderDate.isBefore(endDate)
                            || orderDate.equals(startDate)
                            || orderDate.equals(endDate)) {
                        return true;
                    }
                    return false;
                }).collect(Collectors.toList());

        if (ordersBetweenDates.isEmpty()) {
            throw new NoSuchElementException();
        }
        return ordersBetweenDates;
    }

    public Order getMostExpensiveOrder() {
        List<Order> orders = dataStore.getOrders();
        Order order = orders
                .stream().max(Comparator.comparing(Order::getTotalPrice))
                .orElseThrow(() -> new IllegalArgumentException("List of orders is empty"));
        return order;
    }

    public Food getMostPopularFood() {
        List<Order> orders = dataStore.getOrders();
        Map<Food, Integer> foodAndAmount = getFoodAndAmount(orders);

        Map.Entry<Food, Integer> maxEntry =
                foodAndAmount.entrySet().stream().max(Map.Entry.comparingByValue())
                        .orElseThrow(() -> new IllegalArgumentException("Can't find most popular food"));

        return maxEntry.getKey();
    }

    private Map<Food, Integer> getFoodAndAmount(List<Order> orders) {
        Map<Food, Integer> foodAndAmount = orders.stream()
                .flatMap(order -> order.getOrderItems().stream())
                .collect(Collectors.toMap(
                        OrderItem::getFood
                        , OrderItem::getAmount
                        , Integer::sum));
        return foodAndAmount;
    }

    public long getMostLoyalCustomerId() {
        List<Order> orders = dataStore.getOrders();
        Map<Long, Integer> customerIdCountOfOrder = getCustomerIdCountOfOrder(orders);

        Map.Entry<Long, Integer> entry =
                customerIdCountOfOrder.entrySet().stream().max(Map.Entry.comparingByValue())
                        .orElseThrow(() -> new IllegalArgumentException("Can't find customer id"));

        return entry.getKey();
    }

    private Map<Long, Integer> getCustomerIdCountOfOrder(List<Order> orders) {
        Map<Long, Integer> customerIdCountOfOrder = orders.stream()
                .collect(Collectors.toMap(
                        Order::getCustomerId
                        , order -> 1
                        , Integer::sum));
        return customerIdCountOfOrder;
    }

    public Statistics getStatistics(LocalDate startDate, LocalDate endDate) {
        Statistics statistics = new Statistics();

        List<Order> orders = getOrders(startDate, endDate);
        BigDecimal totalIncome = findTotalIncome(orders);
        statistics.setTotalIncome(totalIncome);

        double averageIncome = totalIncome.doubleValue() / orders.size();
        MathContext context = new MathContext(4, RoundingMode.HALF_UP);
        BigDecimal result = new BigDecimal(averageIncome, context);
        statistics.setAverageIncomeByOrder(result.doubleValue());

        statistics.setNumberOfFood(findNumberOfFood(orders));
        statistics.setNumberOfOrder(orders.size());
        statistics.setDayOfHighestIncome(findDayOfHighestIncome(orders));

        return statistics;
    }

    private BigDecimal findTotalIncome(List<Order> orders) {
        BigDecimal totalIncome = orders
                .stream()
                .map(Order::getTotalPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return totalIncome;
    }

    private int findNumberOfFood(List<Order> orders) {
        int numberOfFood = orders.stream()
                .flatMap(order -> order.getOrderItems().stream())
                .mapToInt(OrderItem::getAmount)
                .reduce(0, Integer::sum);
        return numberOfFood;
    }

    private LocalDate findDayOfHighestIncome(List<Order> orders) {
        Map<LocalDate, BigDecimal> dayAndAmount = getDayAndAmount(orders);

        Map.Entry<LocalDate, BigDecimal> entry =
                dayAndAmount.entrySet().stream().max(Map.Entry.comparingByValue())
                        .orElseThrow(() -> new IllegalArgumentException("Can't find day"));

        return entry.getKey();
    }

    private Map<LocalDate, BigDecimal> getDayAndAmount(List<Order> orders) {
        Map<LocalDate, BigDecimal> dayAndAmount = orders.stream()
                .collect(Collectors.toMap(
                        order -> order.getOrderDate().toLocalDate()
                        , Order::getTotalPrice
                        , BigDecimal::add));
        return dayAndAmount;
    }
}
