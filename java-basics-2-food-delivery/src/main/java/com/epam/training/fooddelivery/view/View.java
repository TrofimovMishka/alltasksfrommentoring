package com.epam.training.fooddelivery.view;

import com.epam.training.fooddelivery.domain.Food;
import com.epam.training.fooddelivery.domain.Order;
import com.epam.training.fooddelivery.domain.Statistics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class View {

    public View() {
    }

    public void printWelcomeMessage() {
        System.out.println("Welcome to Food Delivery Service \n");
    }

    public LocalDate readStartDate() throws IOException {
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("\nEnter the start date (DD-MM-YYYY) : ");
        String startDate = reader.readLine();
        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(startDate, formatter);
        return date;
    }

    public LocalDate readEndDate() throws IOException {
        BufferedReader reader
                = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter the end date (DD-MM-YYYY) : ");
        String endDate = reader.readLine();
        DateTimeFormatter formatter =
                DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDate date = LocalDate.parse(endDate, formatter);
        return date;
    }

    public void printMostExpensiveOrder(Order order) {
        System.out.format("The most expensive order was %d with a total price %d EUR"
                , order.getId(), order.getTotalPrice().intValue());
    }

    public void printMostPopularFood(Food food) {
        System.out.println("\nThe most popular food is " + food.getName());
    }

    public void printMostLoyalCustomer(long customerId) {
        System.out.println("The customer who ordered the most was: #" + customerId);
    }

    public void printStatistics(Statistics statistics, LocalDate startDate, LocalDate endDate) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String dateStart = startDate.format(formatter);
        String dateEnd = endDate.format(formatter);
        int totalIncome = statistics.getTotalIncome().intValue();
        double average = statistics.getAverageIncomeByOrder();
        int numbOfFood = statistics.getNumberOfFood();
        int numbOfOrder = statistics.getNumberOfOrder();
        String day = statistics.getDayOfHighestIncome().format(formatter);

        System.out.format("\nThe statistics between %s and %s: \n" +
                        "The total income was: %d \n" +
                        "The average income per order:  %.2f EUR \n" +
                        "There were %d dishes served \n" +
                        "There were %d orders made \n" +
                        "The day with the highest income: %s"
                , dateStart, dateEnd, totalIncome, average, numbOfFood, numbOfOrder, day);
    }
}
