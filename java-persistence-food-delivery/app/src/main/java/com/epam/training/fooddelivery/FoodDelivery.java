package com.epam.training.fooddelivery;

import com.epam.training.fooddelivery.domain.Customer;
import com.epam.training.fooddelivery.domain.Food;
import com.epam.training.fooddelivery.domain.Order;
import com.epam.training.fooddelivery.domain.User;
import com.epam.training.fooddelivery.service.*;
import com.epam.training.fooddelivery.view.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FoodDelivery implements CommandLineRunner {
    private View view;
    private DefaultFoodService foodService;
    private DefaultCartService cartService;
    private DefaultCustomerService customerService;
    private DefaultOrderService orderService;

    @Override
    public void run(String... args) throws Exception {
        try {
            User user = view.readCredentials();
            Customer customer = customerService.authenticate(user);

            view.printWelcomeMessage(customer);
            List<Food> foods = foodService.listAllFood();
            view.printMessage();

            askUser(customer, foods);
            Order order = getOrder(customer, foods);
            view.printConfirmOrder(order);

        } catch (AuthenticationException | IllegalArgumentException | IllegalStateException ex) {
            System.out.println(ex.getMessage());
        }
    }

    @Autowired
    public void setView(View view) {
        this.view = view;
    }

    @Autowired
    public void setFoodService(DefaultFoodService foodService) {
        this.foodService = foodService;
    }

    @Autowired
    public void setCartService(DefaultCartService cartService) {
        this.cartService = cartService;
    }

    @Autowired
    public void setCustomerService(DefaultCustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setOrderService(DefaultOrderService orderService) {
        this.orderService = orderService;
    }

    private Order getOrder(Customer customer, List<Food> foods) {
        boolean isBalanceEnough = false;
        Order order = null;
        do {
            try {
                order = orderService.createOrder(customer);
                isBalanceEnough = true;
            } catch (LowBalanceException e) {
                view.printErrorMessage(e.getMessage());
                askUser(customer, foods);
            }
        } while (!isBalanceEnough);
        return order;
    }

    private void askUser(Customer customer, List<Food> foods) {
        do {
            view.printAllFoods(foods);
            Food food = getFood(foods);
            int pieces = view.readPieces();
            cartService.updateCart(customer, food, pieces);

            view.printAddedToCard(food, pieces);
            view.printCard(customer.getCart());
        } while (!view.promptOrder());
    }

    private Food getFood(List<Food> foods) {
        boolean isNameCorrect = false;
        Food food = null;
        do {
            try {
                food = view.selectFood(foods);
                isNameCorrect = true;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        } while (!isNameCorrect);
        return food;
    }
}
