package com.epam.training.fooddelivery.service;

import com.epam.training.fooddelivery.domain.Cart;
import com.epam.training.fooddelivery.domain.Customer;
import com.epam.training.fooddelivery.domain.Food;
import com.epam.training.fooddelivery.domain.OrderItem;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class DefaultCartService implements CartService{

    @Override
    public void updateCart(Customer customer, Food food, int pieces) {
        Cart cart = getCart(customer);
        if (pieces == 0) {
            extractOldOrder(food, cart);
        } else {
            BigDecimal price = BigDecimal.valueOf(pieces).multiply(food.getPrice());
            OrderItem item = createOrderItem(food, pieces, price);
            if (cart.getOrderItems() != null) {
                extractOldOrder(food, cart);
            }
            cart.setOrderItems(List.of(item));
            BigDecimal priceCart = cart.getPrice().add(price);
            cart.setPrice(priceCart);
        }
    }

    private void extractOldOrder(Food food, Cart cart) {
        Optional<OrderItem> optional = cart
                .getOrderItems()
                .stream()
                .filter(ele -> ele.getFood().equals(food)).findFirst();
        if (optional.isPresent()) {
            OrderItem oldOrderItem = optional.get();
            BigDecimal priceCart = cart.getPrice().subtract(oldOrderItem.getPrice());
            cart.setPrice(priceCart);
            cart.removeOrderItem(oldOrderItem);
        }
    }

    private OrderItem createOrderItem(Food food, int pieces, BigDecimal price) {
        OrderItem item = new OrderItem();
        item.setPrice(price);
        item.setFood(food);
        item.setPieces(pieces);
        return item;
    }

    private Cart getCart(Customer customer) {
        Cart cart = customer.getCart();
        if (cart == null) {
            cart = new Cart();
            customer.setCart(cart);
        }
        return cart;
    }
}
