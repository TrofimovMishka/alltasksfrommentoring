package com.epam.training.fooddelivery.service;

import com.epam.training.fooddelivery.repository.CustomerRepository;
import com.epam.training.fooddelivery.domain.Customer;
import com.epam.training.fooddelivery.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultCustomerService implements CustomerService{

    private final CustomerRepository customerRepository;

    @Autowired
    public DefaultCustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer authenticate(User user) {
        Customer customer = customerRepository.findByEmailAndPassword(user.getEmail(), user.getPassword());
        if (customer == null){
            throw new AuthenticationException("Invalid username or password");
        }
        return customer;
    }
}
