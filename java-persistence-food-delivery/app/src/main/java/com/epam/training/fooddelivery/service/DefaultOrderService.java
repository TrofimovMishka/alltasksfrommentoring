package com.epam.training.fooddelivery.service;

import com.epam.training.fooddelivery.repository.CustomerRepository;
import com.epam.training.fooddelivery.repository.OrderItemRepository;
import com.epam.training.fooddelivery.repository.OrderRepository;
import com.epam.training.fooddelivery.domain.Customer;
import com.epam.training.fooddelivery.domain.Order;
import com.epam.training.fooddelivery.domain.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class DefaultOrderService implements OrderService{

    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;
    private final CustomerRepository customerRepository;

    @Autowired
    public DefaultOrderService(OrderRepository orderRepository, OrderItemRepository orderItemRepository, CustomerRepository customerRepository) {
        this.orderRepository = orderRepository;
        this.orderItemRepository = orderItemRepository;
        this.customerRepository = customerRepository;
    }

    @Override
    @Transactional
    public Order createOrder(Customer customer) {

        BigDecimal priceOfCart = customer.getCart().getPrice();
        BigDecimal balance = customer.getBalance();
        moneyVerification(priceOfCart, balance);

        List<OrderItem> orderItems =  customer.getCart().getOrderItems();

        Order order = new Order();
        order.setCustomer(customer);
        order.setPrice(priceOfCart);
        order.setTimestampCreated(LocalDateTime.now());
        order.setOrderItems(orderItems);

        orderItems.forEach(orderItem -> orderItem.setOrder(order));

        customer.setBalance(balance.subtract(priceOfCart));
        customer.setCart(null);
        customer.setOrders(List.of(order));

        orderRepository.saveAndFlush(order);
        customerRepository.saveAndFlush(customer);
        orderItemRepository.saveAllAndFlush(orderItems);

        return order;
    }

    private void moneyVerification(BigDecimal priceOfCart, BigDecimal customersMoney) {
        if (priceOfCart.compareTo(BigDecimal.ZERO) == 0) {
            throw new IllegalStateException();
        }
        if (customersMoney.compareTo(priceOfCart) < 0) {
            throw new LowBalanceException(customersMoney.toString());
        }
    }
}
