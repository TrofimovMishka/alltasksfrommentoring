#!/usr/bin/env bash

current="population/current-population.txt"
prev="population/previous-population.txt"
touch results.csv
file=$(pwd)/results.csv

cd input/

for country in *
do	
	cd "$country" 
	myCity=" "
	maxPop="-1"
		for city in *
		do	
			declare -i cPop=$(cat "$city/$current")
			declare -i pPop=$(cat "$city/$prev")

			sub=$(( cPop - pPop ))
			div=$(echo "scale=4; $sub / $pPop" | bc)
			result=$(printf "%.2f" $(echo "scale=2; $div * 100.0 " | bc))
			
			if [[ $(echo "$result > $maxPop" | bc) -eq 1 ]]; then
				maxPop=$result
				myCity=$city
			fi
		done
	cd ..
	echo "$country,$myCity,$maxPop%" >> $file
done
			
