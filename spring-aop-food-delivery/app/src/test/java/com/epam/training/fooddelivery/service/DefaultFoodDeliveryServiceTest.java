package com.epam.training.fooddelivery.service;

import com.epam.training.fooddelivery.data.FileDataStore;
import com.epam.training.fooddelivery.domain.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DefaultFoodDeliveryServiceTest {
    // change the code to delete the static fields
    private static final String REGULAR_PRICE_FIDEUA = "15";
    private static final String PRICE_FOR_ORDERITEM = "30";
    private static final String ENOUGH_BALANCE_FOR_CUSTOMER = "1000";
    private static final String PRICE_FOR_CART = "100";
    private static final String NOT_ENOUGH_BALANCE_FOR_CUSTOMER = "10";
    private static final String REGULAR_PRICE_PEALLA = "13";
    private static FileDataStore dataStoreMock;
    private static DefaultFoodDeliveryService service;
    private static List<Food> listOfFoods = new ArrayList<>();
    private static List<Customer> listOfCustomers = new ArrayList<>();
    private Food food;
    private Customer customer;
    private Order order;
    private OrderItem orderItem;

    @BeforeAll
    static void createService() {
        dataStoreMock = mock(FileDataStore.class);
        service = new DefaultFoodDeliveryService(dataStoreMock);
        fillWithFoods();
        fillWithCustomers();
    }

    @BeforeEach
    void setUp() {
        createTestFood();
        createTestOrderItem();
        createTestOrder();
        createTestCustomer();
    }

    private void createTestOrderItem() {
        orderItem = new OrderItem();
        orderItem.setFood(food);
        orderItem.setPieces(2);
        orderItem.setPrice(new BigDecimal(PRICE_FOR_ORDERITEM));
    }

    private void createTestOrder() {
        order = new Order();
        order.setOrderId(1L);
        order.setOrderItems(List.of(orderItem));
    }

    private void createTestCustomer() {
        customer = new Customer();
        customer.setBalance(new BigDecimal(ENOUGH_BALANCE_FOR_CUSTOMER));
        customer.setId(1);

        Cart testCart = new Cart();
        testCart.setPrice(new BigDecimal(PRICE_FOR_CART));
        testCart.setOrderItems(List.of(orderItem));

        customer.setCart(testCart);
        customer.setOrders(List.of(order));
    }

    private void createTestFood() {
        food = new Food();
        food.setName("Fideua");
        food.setPrice(new BigDecimal(REGULAR_PRICE_FIDEUA));
    }

    private static void fillWithCustomers() {
        Customer customer1 = new Customer();
        customer1.setEmail("email1");
        customer1.setName("password1");

        Customer customer2 = new Customer();
        customer2.setEmail("email2");
        customer2.setPassword("password2");

        listOfCustomers = List.of(customer1, customer2);
    }

    private static void fillWithFoods() {
        List<String> names = List.of("Fideua", "Paella", "Tortilla", "Gazpacho", "Quesadilla");
        for (int i = 0; i < names.size() - 1; i++) {
            Food food = new Food();
            food.setName(names.get(i));
            listOfFoods.add(food);
        }
    }

    @Test
    void returnListOfAllAvailableFoods() {
        when(dataStoreMock.getFoods()).thenReturn(listOfFoods);
        List<Food> actualListOfFood = service.listAllFood();

        assertEquals(listOfFoods, actualListOfFood);
    }

    @Test
    void createCustomerIfEmailAndPasswordIsCorrect() {
        User testUser = new User();
        testUser.setEmail("email2");
        testUser.setPassword("password2");

        Customer expectedCustomer = new Customer();
        expectedCustomer.setEmail("email2");
        expectedCustomer.setPassword("password2");

        when(dataStoreMock.getCustomers()).thenReturn(listOfCustomers);
        Customer actualCustomer = service.authenticate(testUser);

        assertEquals(expectedCustomer, actualCustomer);
    }

    @Test
    void failAuthenticationIfEmailIsIncorrect() throws AuthenticationException {
        User testUser = new User();
        testUser.setEmail("incorrectEmail");
        testUser.setPassword("password2");

        when(dataStoreMock.getCustomers()).thenReturn(listOfCustomers);

        assertThrows(AuthenticationException.class, () -> {
            service.authenticate(testUser);
        });

    }

    @Test
    void failAuthenticationIfPasswordIsIncorrect() throws AuthenticationException {
        User testUser = new User();
        testUser.setEmail("email2");
        testUser.setPassword("incorrectPassword");

        when(dataStoreMock.getCustomers()).thenReturn(listOfCustomers);

        assertThrows(AuthenticationException.class, () -> {
            service.authenticate(testUser);
        });

    }

    @Test
    void createOrderIfEnoughCustomersBalance() {
        customer.setBalance(new BigDecimal(ENOUGH_BALANCE_FOR_CUSTOMER));
        when(dataStoreMock.createOrder(any())).thenReturn(order);

        Order actualOrder = service.createOrder(customer);

        assertEquals(order, actualOrder);
    }

    @Test
    void failToCreateOrderIfNotEnoughCustomersBalance() throws LowBalanceException {
        customer.setBalance(new BigDecimal(NOT_ENOUGH_BALANCE_FOR_CUSTOMER));
        Order customerOrder = customer.getOrders().get(0);
        customerOrder.setPrice(new BigDecimal(ENOUGH_BALANCE_FOR_CUSTOMER));

        assertThrows(LowBalanceException.class, () -> {
            service.createOrder(customer);
        });
    }

    @Test
    void failToCreateOrderIfCartHaveZeroPrice() throws IllegalStateException {
        customer.getCart().setPrice(BigDecimal.ZERO);

        assertThrows(IllegalStateException.class, () -> {
            service.createOrder(customer);
        });

    }

    @Test
    void removeAKindOfFoodFromTheCart() {
        service.updateCart(customer, food, 0);

        assertEquals(0, customer.getCart().getOrderItems().size());
    }

    @Test
    void changeTheNumberOfPiecesOfAnItemInTheCart() {
        List<OrderItem> actualOrderItems = customer.getCart().getOrderItems();
        service.updateCart(customer, food, 3);

        assertEquals(3, actualOrderItems.get(0).getPieces());
    }

    @Test
    void modifyTheNumberOfOrderItems() {
        Food additionalFoodFake = new Food();
        additionalFoodFake.setName("Paella");
        additionalFoodFake.setPrice(new BigDecimal(REGULAR_PRICE_PEALLA));

        service.updateCart(customer, additionalFoodFake, 1);

        assertEquals(2, customer.getCart().getOrderItems().size());
    }

    @Test
    void createNewCartWhenCustomerIsNew() {
        Customer customer = new Customer();
        assertNull(customer.getCart());

        service.updateCart(customer, food, 1);

        assertNotNull(customer.getCart());
    }
}
