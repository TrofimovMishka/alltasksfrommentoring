package com.epam.training.fooddelivery;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "com.epam.training.fooddelivery")
@PropertySource(value = "classpath:application.properties")
public class ApplicationConfig { }

