/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech) (5.3.0).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
package com.epam.training.fooddelivery.api;

import com.epam.training.fooddelivery.model.FoodModel;
import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;
import java.util.Optional;
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2022-08-10T16:16:50.107161500+02:00[Europe/Warsaw]")
@Validated
@Api(value = "foodservise", description = "the foodservise API")
public interface FoodserviseApi {

    /**
     * GET /foodservise/foods : List all foods
     * Show list of all foods available in foodservice
     *
     * @return A list of foods (status code 200)
     */
    @ApiOperation(value = "List all foods", nickname = "foodserviseFoodsGet", notes = "Show list of all foods available in foodservice", response = FoodModel.class, responseContainer = "List", tags={ "food-controller", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "A list of foods", response = FoodModel.class, responseContainer = "List") })
    @RequestMapping(
        method = RequestMethod.GET,
        value = "/foodservise/foods",
        produces = { "application/json" }
    )
    ResponseEntity<List<FoodModel>> foodserviseFoodsGet();

}
