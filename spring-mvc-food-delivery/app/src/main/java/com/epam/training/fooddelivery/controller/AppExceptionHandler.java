package com.epam.training.fooddelivery.controller;

import com.epam.training.fooddelivery.exception.DataDoesNotExistException;
import com.epam.training.fooddelivery.exception.DataNotBelongCustomerException;
import com.epam.training.fooddelivery.exception.EmptyException;
import com.epam.training.fooddelivery.exception.LowBalanceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler(LowBalanceException.class)
    public ResponseEntity<String> handleLowBalanceExceptions(Exception exception) {
        HttpStatus status = HttpStatus.valueOf(400);
        return new ResponseEntity<>(exception.getMessage(), status);
    }

    @ExceptionHandler(EmptyException.class)
    public ResponseEntity<String> handleEmptyExceptions(Exception exception) {
        HttpStatus status = HttpStatus.valueOf(400);
        return new ResponseEntity<>(exception.getMessage(), status);
    }

    @ExceptionHandler(DataDoesNotExistException.class)
    public ResponseEntity<String> handleDataDoesNotExistExceptions(Exception exception) {
        HttpStatus status = HttpStatus.valueOf(404);
        return new ResponseEntity<>(exception.getMessage(), status);
    }

    @ExceptionHandler(DataNotBelongCustomerException.class)
    public ResponseEntity<String> handleDataNotBelongCustomerExceptions(Exception exception) {
        HttpStatus status = HttpStatus.valueOf(403);
        return new ResponseEntity<>(exception.getMessage(), status);
    }

}
