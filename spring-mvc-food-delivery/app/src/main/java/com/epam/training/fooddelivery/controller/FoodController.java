package com.epam.training.fooddelivery.controller;

import com.epam.training.fooddelivery.api.FoodserviseApi;
import com.epam.training.fooddelivery.converter.FoodConverter;
import com.epam.training.fooddelivery.model.FoodModel;
import com.epam.training.fooddelivery.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class FoodController implements FoodserviseApi {
    private FoodService foodService;
    private FoodConverter foodConverter;

    @Override
    @GetMapping("/foodservice/foods")
    public ResponseEntity<List<FoodModel>> foodserviseFoodsGet() {
        List<FoodModel> foodModels = foodService.listAllFood()
                .stream()
                .map(food -> foodConverter.convert(food))
                .collect(Collectors.toList());
        return new ResponseEntity<>(foodModels, HttpStatus.OK);
    }

    @Autowired
    public void setFoodConverter(FoodConverter foodConverter) {
        this.foodConverter = foodConverter;
    }

    @Autowired
    public void setFoodService(FoodService foodService) {
        this.foodService = foodService;
    }
}
