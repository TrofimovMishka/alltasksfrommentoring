package com.epam.training.fooddelivery.controller;

import com.epam.training.fooddelivery.api.OrderserviseApi;
import com.epam.training.fooddelivery.converter.CartConverter;
import com.epam.training.fooddelivery.converter.OrderConverter;
import com.epam.training.fooddelivery.domain.Customer;
import com.epam.training.fooddelivery.domain.Order;
import com.epam.training.fooddelivery.exception.DataDoesNotExistException;
import com.epam.training.fooddelivery.exception.DataNotBelongCustomerException;
import com.epam.training.fooddelivery.exception.EmptyException;
import com.epam.training.fooddelivery.exception.LowBalanceException;
import com.epam.training.fooddelivery.model.CartModel;
import com.epam.training.fooddelivery.model.OrderModel;
import com.epam.training.fooddelivery.service.CustomerService;
import com.epam.training.fooddelivery.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/orderservice")
public class OrderController implements OrderserviseApi {
    private CustomerService customerService;
    private OrderService orderService;
    private OrderConverter orderConverter;
    private CartConverter cartConverter;

    @Override
    @GetMapping("/orders")
    public ResponseEntity<List<OrderModel>> orderserviseOrdersGet() {
        Customer customer = getCustomer();
        List<Order> orders = customer.getOrders();

        List<OrderModel> orderModels = orders
                .stream()
                .map(order -> orderConverter.convert(order))
                .collect(Collectors.toList());
        return new ResponseEntity<>(orderModels, HttpStatus.OK);
    }

    @Override
    @GetMapping("/orders/{orderId}")
    public ResponseEntity<OrderModel> orderserviseOrdersOrderIdGet(@PathVariable("orderId") Long orderId) {
        Customer customer = getCustomer();
        Order order = orderService.getOrderById(orderId);

        if (order == null) { throw new DataDoesNotExistException("The order does not exist"); }

        if (!customer.equals(order.getCustomer())) {
            throw new DataNotBelongCustomerException("The order exists but does not belong to the authenticated customer");
        }
        OrderModel orderModel = orderConverter.convert(order);
        return new ResponseEntity<>(orderModel, HttpStatus.OK);
    }

    @Override
    @PostMapping("/orders")
    public ResponseEntity<OrderModel> orderserviseOrdersPost(CartModel cartModel) {
        if (cartModel == null
                || cartModel.getPrice() == null
                || cartModel.getOrderItemModels() == null
                || cartModel.getPrice() == 0
                || cartModel.getOrderItemModels().size() == 0) {
            throw new EmptyException("The Cart is empty");
        }

        Customer customer = getCustomer();
        customer.setCart(cartConverter.convert(cartModel));
        OrderModel orderModel;

        try {
            Order order = orderService.createOrder(customer);
            orderModel = orderConverter.convert(order);

        } catch (LowBalanceException | IllegalArgumentException ex) {
            throw new LowBalanceException("Balance of authenticated customer is not enough");
        }
        return new ResponseEntity<>(orderModel, HttpStatus.OK);
    }

    private Customer getCustomer() {
        UserDetails userPrincipal = (UserDetails) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        String email = userPrincipal.getUsername();
        return customerService.findCustomerByEmail(email);
    }

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @Autowired
    public void setOrderConverter(OrderConverter orderConverter) {
        this.orderConverter = orderConverter;
    }

    @Autowired
    public void setCartConverter(CartConverter cartConverter) {
        this.cartConverter = cartConverter;
    }
}
