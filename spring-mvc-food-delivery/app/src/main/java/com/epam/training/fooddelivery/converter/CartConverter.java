package com.epam.training.fooddelivery.converter;

import com.epam.training.fooddelivery.domain.Cart;
import com.epam.training.fooddelivery.domain.OrderItem;
import com.epam.training.fooddelivery.model.CartModel;
import com.epam.training.fooddelivery.model.OrderItemModel;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class CartConverter implements Converter<Cart, CartModel> {
    private OrderItemConverter orderItemConverter;

    @Override
    public CartModel convert(Cart cart) {
        CartModel cartModel = new CartModel();

        cartModel.setPrice(cart.getPrice().longValue());
        List<OrderItemModel> orderItemModels = cart.getOrderItems()
                .stream()
                .map(orderItem -> orderItemConverter.convert(orderItem))
                .collect(Collectors.toList());
        cartModel.setOrderItemModels(orderItemModels);

        return cartModel;
    }

    public Cart convert(CartModel cartModel){
        Cart cart = new Cart();

        cart.setPrice(BigDecimal.valueOf(cartModel.getPrice()));
        List<OrderItem> orderItems = cartModel.getOrderItemModels()
                .stream()
                .map(orderItemModel -> orderItemConverter.convert(orderItemModel))
                .collect(Collectors.toList());
        cart.setOrderItems(orderItems);

        return cart;
    }

    @Autowired
    public void setOrderItemConverter(OrderItemConverter orderItemConverter) {
        this.orderItemConverter = orderItemConverter;
    }
}
