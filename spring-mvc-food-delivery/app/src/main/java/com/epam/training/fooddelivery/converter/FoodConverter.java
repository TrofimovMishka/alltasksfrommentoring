package com.epam.training.fooddelivery.converter;

import com.epam.training.fooddelivery.domain.Category;
import com.epam.training.fooddelivery.domain.Food;
import com.epam.training.fooddelivery.model.FoodModel;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

import java.math.BigDecimal;

@Component
public class FoodConverter implements Converter<Food, FoodModel>{

    @Override
    public FoodModel convert(Food food){
        FoodModel foodModel = new FoodModel();

        foodModel.setId(food.getId());
        foodModel.setName(food.getName());
        foodModel.setCalorie(food.getCalorie().longValue());
        foodModel.setDescription(food.getDescription());
        foodModel.setPrice(food.getPrice().longValue());
        foodModel.setCategory(food.getCategory().name());

        return foodModel;
    }

    public Food convert(FoodModel foodModel){
        Food food = new Food();

        food.setId(foodModel.getId());
        food.setName(foodModel.getName());
        food.setCalorie(BigDecimal.valueOf(foodModel.getCalorie()));
        food.setDescription(foodModel.getDescription());
        food.setPrice(BigDecimal.valueOf(foodModel.getPrice()));
        food.setCategory(convertCategory(foodModel.getCategory()));

        return food;
    }

    private Category convertCategory(String category) {
        return switch (category) {
            case "DAIRY" -> Category.DAIRY;
            case "FRUIT" -> Category.FRUIT;
            case "GRAINS" -> Category.GRAINS;
            case "MEAT" -> Category.MEAT;
            case "SNACK" -> Category.SNACK;
            case "VEGETABLE" -> Category.VEGETABLE;
            default -> Category.MEAL;
        };
    }

}
