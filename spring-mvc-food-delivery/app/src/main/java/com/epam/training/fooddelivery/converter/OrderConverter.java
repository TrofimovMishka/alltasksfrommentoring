package com.epam.training.fooddelivery.converter;

import com.epam.training.fooddelivery.domain.Order;
import com.epam.training.fooddelivery.model.OrderItemModel;
import com.epam.training.fooddelivery.model.OrderModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class OrderConverter implements Converter<Order, OrderModel> {
    private OrderItemConverter orderItemConverter;

    @Override
    public OrderModel convert(Order order) {
        OrderModel orderModel = new OrderModel();

        orderModel.setId(order.getId());
        orderModel.setPrice(order.getPrice().longValue());
        orderModel.setTimestampCreated(order.getTimestampCreated());

        List<OrderItemModel> orderItemModels = order.getOrderItems()
                .stream()
                .map(orderItem -> orderItemConverter.convert(orderItem))
                .collect(Collectors.toList());
        orderModel.setOrderItemModels(orderItemModels);

        return orderModel;
    }

    @Autowired
    public void setOrderItemConverter(OrderItemConverter orderItemConverter) {
        this.orderItemConverter = orderItemConverter;
    }
}
