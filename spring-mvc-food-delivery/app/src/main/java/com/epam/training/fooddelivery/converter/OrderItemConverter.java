package com.epam.training.fooddelivery.converter;

import com.epam.training.fooddelivery.domain.OrderItem;
import com.epam.training.fooddelivery.model.OrderItemModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.core.convert.converter.Converter;

import java.math.BigDecimal;

@Component
public class OrderItemConverter implements Converter<OrderItem, OrderItemModel>{
    private FoodConverter foodConverter;

    @Override
    public OrderItemModel convert(OrderItem orderItem){
        OrderItemModel orderItemModel = new OrderItemModel();

        orderItemModel.setId(orderItem.getId());
        orderItemModel.setFoodModel(foodConverter.convert(orderItem.getFood()));
        orderItemModel.setPieces(orderItem.getPieces());
        orderItemModel.setPrice(orderItem.getPrice().longValue());

        return orderItemModel;
    }

    public OrderItem convert(OrderItemModel orderItemModel){
        OrderItem orderItem = new OrderItem();

        orderItem.setId(orderItemModel.getId());
        orderItem.setFood(foodConverter.convert(orderItemModel.getFoodModel()));
        orderItem.setPieces(orderItemModel.getPieces());
        orderItem.setPrice(BigDecimal.valueOf(orderItemModel.getPrice()));

        return orderItem;
    }

    @Autowired
    public void setFoodConverter(FoodConverter foodConverter) {
        this.foodConverter = foodConverter;
    }
}
