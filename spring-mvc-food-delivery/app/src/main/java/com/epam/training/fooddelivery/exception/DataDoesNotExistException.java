package com.epam.training.fooddelivery.exception;

public class DataDoesNotExistException extends RuntimeException{
    public DataDoesNotExistException(String message) {
        super(message);
    }
}
