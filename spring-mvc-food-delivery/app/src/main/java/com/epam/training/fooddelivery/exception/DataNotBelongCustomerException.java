package com.epam.training.fooddelivery.exception;

public class DataNotBelongCustomerException extends RuntimeException{
    public DataNotBelongCustomerException(String message) {
        super(message);
    }
}
