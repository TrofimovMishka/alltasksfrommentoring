package com.epam.training.fooddelivery.exception;

public class EmptyException extends RuntimeException{
    public EmptyException(String message) {
        super(message);
    }
}
