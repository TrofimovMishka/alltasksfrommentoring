package com.epam.training.fooddelivery.service;

import com.epam.training.fooddelivery.exception.AuthenticationException;
import com.epam.training.fooddelivery.repository.CustomerRepository;
import com.epam.training.fooddelivery.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DefaultCustomerService implements CustomerService{

    private final CustomerRepository customerRepository;

    @Autowired
    public DefaultCustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer findCustomerByEmail(String email) {
        Customer customer = customerRepository.findByEmail(email);
        if (customer == null){
            throw new AuthenticationException("Invalid username or password");
        }
        return customer;
    }
}
