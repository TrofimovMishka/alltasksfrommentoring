package com.epam.training.fooddelivery.service;

import com.epam.training.fooddelivery.domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailsService implements UserDetailsService {

    private CustomerService customerService;

    @Override
    public UserDetails loadUserByUsername(String email) {
        Customer customer = customerService.findCustomerByEmail(email);
        if (customer == null) {
            throw new UsernameNotFoundException(email);
        }
        return new MyUserPrincipal(customer);
    }

    @Autowired
    public void setCustomerService(CustomerService customerService) {
        this.customerService = customerService;
    }
}
