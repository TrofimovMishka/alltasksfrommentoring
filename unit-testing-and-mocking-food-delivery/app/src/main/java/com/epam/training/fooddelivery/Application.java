package com.epam.training.fooddelivery;

import com.epam.training.fooddelivery.data.FileDataStore;
import com.epam.training.fooddelivery.domain.Customer;
import com.epam.training.fooddelivery.domain.Food;
import com.epam.training.fooddelivery.domain.Order;
import com.epam.training.fooddelivery.domain.User;
import com.epam.training.fooddelivery.service.AuthenticationException;
import com.epam.training.fooddelivery.service.DefaultFoodDeliveryService;
import com.epam.training.fooddelivery.service.LowBalanceException;
import com.epam.training.fooddelivery.view.CLIView;
import com.epam.training.fooddelivery.view.View;

import java.util.List;

public class Application {

    public static void main(String[] args) {
        Application application = new Application();
        if(args.length < 1){
            throw new IllegalArgumentException("Application use path to files. But path is unavailable");
        }
        application.start(args[0]);
    }

    private void start(String path) {
        try {
            View view = new CLIView();
            User user = view.readCredentials();
            FileDataStore fileDataStore =
                    new FileDataStore(path);
            fileDataStore.init();
            DefaultFoodDeliveryService service = new DefaultFoodDeliveryService(fileDataStore);

            Customer customer = service.authenticate(user);
            view.printWelcomeMessage(customer);
            List<Food> foods = service.listAllFood();
            view.printMessage();

            askUser(view, service, customer, foods);
            Order order = getOrder(view, service, customer, foods);
            view.printConfirmOrder(order);

        } catch (AuthenticationException | IllegalArgumentException | IllegalStateException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private Order getOrder(View view, DefaultFoodDeliveryService service, Customer customer, List<Food> foods) {
        boolean isBalanceEnough = false;
        Order order = null;
        do {
            try {
                order = service.createOrder(customer);
                isBalanceEnough = true;
            } catch (LowBalanceException e) {
                view.printErrorMessage(e.getMessage());
                askUser(view, service, customer, foods);
            }
        } while (!isBalanceEnough);
        return order;
    }

    private void askUser(View view, DefaultFoodDeliveryService service, Customer customer, List<Food> foods) {
        do {
            view.printAllFoods(foods);
            Food food = getFood(view, foods);
            int pieces = view.readPieces();
            service.updateCart(customer, food, pieces);

            view.printAddedToCard(food, pieces);
            view.printCard(customer.getCart());
        } while (!view.promptOrder());
    }

    private Food getFood(View view, List<Food> foods) {
        boolean isNameCorrect = false;
        Food food = null;
        do {
            try {
                food = view.selectFood(foods);
                isNameCorrect = true;
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
            }
        } while (!isNameCorrect);
        return food;
    }
}
