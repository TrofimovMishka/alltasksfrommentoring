package com.epam.training.fooddelivery.service;

import com.epam.training.fooddelivery.data.DataStore;
import com.epam.training.fooddelivery.domain.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DefaultFoodDeliveryServiceTest {
    // change the code to delete the static fields
    private static final String REGULAR_PRICE_FIDEUA = "15";
    private static final String PRICE_FOR_ORDERITEM = "30";
    private static final String ENOUGH_BALANCE_FOR_CUSTOMER = "1000";
    private static final String PRICE_FOR_CART = "100";
    private static final String NOT_ENOUGH_BALANCE_FOR_CUSTOMER = "10";
    private static final String REGULAR_PRICE_PEALLA = "13";
    private static DataStore dataStoreMock;
    private static DefaultFoodDeliveryService service;
    private static List<Food> listOfFoodsFake = new ArrayList<>();
    private static List<Customer> listOfCustomersFake = new ArrayList<>();
    private Food foodFake;
    private Customer customerFake;
    private Order orderFake;
    private OrderItem orderItemFake;
// delete the fake word in naming
    @BeforeAll
    static void createService() {
        dataStoreMock = mock(DataStore.class);
        service = new DefaultFoodDeliveryService(dataStoreMock);
        fillWithFoods();
        fillWithCustomers();
    }

    @BeforeEach
    void setUp() {
        createFoodFake();
        createOrderItemFake();
        createOrderFake();
        createCustomerFake();
    }

    private void createOrderItemFake() {
        orderItemFake = new OrderItem();
        orderItemFake.setFood(foodFake);
        orderItemFake.setPieces(2);
        orderItemFake.setPrice(new BigDecimal(PRICE_FOR_ORDERITEM));
    }

    private void createOrderFake() {
        orderFake = new Order();
        orderFake.setOrderId(1L);
        orderFake.setOrderItems(List.of(orderItemFake));
    }

    private void createCustomerFake() {
        customerFake = new Customer();
        customerFake.setBalance(new BigDecimal(ENOUGH_BALANCE_FOR_CUSTOMER));
        customerFake.setId(1);

        Cart testCart = new Cart();
        testCart.setPrice(new BigDecimal(PRICE_FOR_CART));
        testCart.setOrderItems(List.of(orderItemFake));

        customerFake.setCart(testCart);
    }

    private void createFoodFake() {
        foodFake = new Food();
        foodFake.setName("Fideua");
        foodFake.setPrice(new BigDecimal(REGULAR_PRICE_FIDEUA));
    }

    private static void fillWithCustomers() {
        Customer customerFake1 = new Customer();
        customerFake1.setEmail("emailFake1");
        customerFake1.setName("passwordFake1");

        Customer customerFake2 = new Customer();
        customerFake2.setEmail("emailFake2");
        customerFake2.setPassword("passwordFake2");

        listOfCustomersFake = List.of(customerFake1, customerFake2);
    }

    private static void fillWithFoods() {
        List<String> names = List.of("Fideua", "Paella", "Tortilla", "Gazpacho", "Quesadilla");
        for (int i = 0; i < names.size() - 1; i++) {
            Food food = new Food();
            food.setName(names.get(i));
            listOfFoodsFake.add(food);
        }
    }

    @Test
    void returnListOfAllAvailableFoods() {
        when(dataStoreMock.getFoods()).thenReturn(listOfFoodsFake);

        List<Food> actualListOfFood = service.listAllFood();

        assertEquals(listOfFoodsFake, actualListOfFood);
    }

    @Test
    void createCustomerIfEmailAndPasswordIsCorrect() {
        User testUser = new User();
        testUser.setEmail("emailFake2");
        testUser.setPassword("passwordFake2");

        Customer expectedCustomer = new Customer();
        expectedCustomer.setEmail("emailFake2");
        expectedCustomer.setPassword("passwordFake2");

        when(dataStoreMock.getCustomers()).thenReturn(listOfCustomersFake);
        Customer actualCustomer = service.authenticate(testUser);

        assertEquals(expectedCustomer, actualCustomer);
    }

    @Test
    void failAuthenticationIfEmailIsIncorrect() throws AuthenticationException {
        User testUser = new User();
        testUser.setEmail("incorrectEmail");
        testUser.setPassword("passwordFake2");

        when(dataStoreMock.getCustomers()).thenReturn(listOfCustomersFake);

        Throwable thrown = assertThrows(AuthenticationException.class, () -> {
            service.authenticate(testUser);
        });
        assertNotNull(thrown.getStackTrace());
    }

    @Test
    void failAuthenticationIfPasswordIsIncorrect() throws AuthenticationException {
        User testUser = new User();
        testUser.setEmail("emailFake2");
        testUser.setPassword("incorrectPassword");

        when(dataStoreMock.getCustomers()).thenReturn(listOfCustomersFake);

        Throwable thrown = assertThrows(AuthenticationException.class, () -> {
            service.authenticate(testUser);
        });
        assertNotNull(thrown.getStackTrace());
    }
// we not see any balance in this
    @Test
    void createOrderIfEnoughCustomersBalance() {
        when(dataStoreMock.createOrder(any())).thenReturn(orderFake);
        Order actualOrder = service.createOrder(customerFake);

        assertEquals(orderFake, actualOrder);
    }

    @Test
    void failToCreateOrderIfNotEnoughCustomersBalance() throws LowBalanceException {
        customerFake.setBalance(new BigDecimal(NOT_ENOUGH_BALANCE_FOR_CUSTOMER));
// what the amount order here
        Throwable thrown = assertThrows(LowBalanceException.class, () -> {
            service.createOrder(customerFake);
        });
        assertNotNull(thrown.getStackTrace());
    }
// change cart is empty naming. use size of cart
    @Test
    void failToCreateOrderIfCartIsEmpty() throws IllegalStateException{
        customerFake.getCart().setPrice(BigDecimal.ZERO);

        Throwable thrown = assertThrows(IllegalStateException.class, () -> {
            service.createOrder(customerFake);
        });
        assertNotNull(thrown.getStackTrace());
    }

    @Test
    void removeAKindOfFoodFromTheCart() {

        service.updateCart(customerFake, foodFake, 0);
// add few foods in cart. create cart in this method
        assertEquals(0, customerFake.getCart().getOrderItems().size());
    }

    @Test
    void changeTheNumberOfPiecesOfAnItemInTheCart() {
        List<OrderItem> actualOrderItems = customerFake.getCart().getOrderItems();
        //check what we remove the current food not another. Name for foodFake should be more readeble
        service.updateCart(customerFake, foodFake, 3);

        assertEquals(3, actualOrderItems.get(0).getPieces());
    }

    @Test
    void modifyTheNumberOfOrderItems() {
        Food additionalFoodFake = new Food();
        additionalFoodFake.setName("Paella");
        additionalFoodFake.setPrice(new BigDecimal(REGULAR_PRICE_PEALLA));

        service.updateCart(customerFake, additionalFoodFake, 1);

        assertEquals(2, customerFake.getCart().getOrderItems().size());
    }

    @Test
    void createNewCartWhenCustomerIsNew() {
        customerFake.setCart(null);
// create new customer in this place
        service.updateCart(customerFake, foodFake, 1);

        assertNotNull(customerFake.getCart());
    }
}
